const divTime = document.querySelector('.content-timeout')
const btn = document.querySelector('.btn-timeout')
const divInter = document.querySelector('.content-interval')
let i = 11

document.addEventListener("DOMContentLoaded" , ()=>{
let intId = setInterval(()=>{
    divInter.innerHTML = --i
    if(i == 0){
        clearInterval(intId)
        divInter.innerHTML = "Зворотній відлік завершено"
        setTimeout(()=>{
            divInter.style.display = "none"
            divTime.style.display = "block"
            btn.style.display = "block"
        } , 1000)
    }
} , 1000)})


btn.addEventListener('click' , ()=>{setTimeout(()=>{
    divTime.innerHTML = "Таймер спрацював успішно"
} , 3000)} , {once : true})